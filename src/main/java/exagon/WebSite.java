package exagon;

import microservices.IGui;
import microservices.IPerson;
import microservices.ITrigger;

public class WebSite {

    private final IGui gui;
    private final IPerson person;
    private final ITrigger trigger;

    public WebSite(WebSiteBuilder webSiteBuilder) {
        this.gui = webSiteBuilder.gui;
        this.person = webSiteBuilder.person;
        this.trigger = webSiteBuilder.trigger;
    }

    public void showPage(){
        int l1 = person.firstName().length();
        int l2 = person.lastName().length();
        int l3 = person.age().length();
        int maxLen = 0;
        if(l1 > l2)
            maxLen = l1;
        else
            maxLen = l2;

        if(l3 > maxLen)
            maxLen = l3;

        gui.print(person.firstName(), maxLen);
        gui.print(person.lastName(), maxLen);
        gui.print(person.age(), maxLen);
    }

    public void operation(int num){
        if(num == trigger.getId())
            trigger.execute();
        else
            System.out.println("... nothing happen");
    }
}
