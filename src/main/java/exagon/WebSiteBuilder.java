package exagon;

import microservices.IGui;
import microservices.IPerson;
import microservices.ITrigger;

class WebSiteBuilder {

    IGui gui;
    IPerson person;
    ITrigger trigger;

    WebSiteBuilder setGui(IGui gui) {
        this.gui = gui;
        return this;
    }

    WebSiteBuilder setPerson(IPerson person) {
        this.person = person;
        return this;
    }

    WebSiteBuilder setTrigger(ITrigger trigger) {
        this.trigger = trigger;
        return this;
    }

    WebSite build(){
        if(gui == null)
            throw new IllegalArgumentException("Error: gui can not be null");
        if(person == null)
            throw new IllegalArgumentException("Error: person can not be null");
        if(trigger == null)
            throw new IllegalArgumentException("Error: trigger can not be null");

        return new WebSite(this);
    }
}
