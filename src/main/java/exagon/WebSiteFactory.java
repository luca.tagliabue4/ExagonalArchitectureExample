package exagon;

import microservices.Citizen;
import microservices.DashGUI;
import microservices.DummyTrigger;

public class WebSiteFactory {

    public static WebSite createWebSiteStandard(String firstName, String lastName, String age){
        return new WebSiteBuilder().setGui(new DashGUI())
                .setPerson(new Citizen(firstName, lastName, age))
                .setTrigger(new DummyTrigger())
                .build();
    }
}
