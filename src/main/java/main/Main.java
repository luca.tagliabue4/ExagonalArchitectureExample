package main;

import exagon.WebSite;
import exagon.WebSiteFactory;
import microservices.*;

public class Main {
    public static void main(String[] args) {
        WebSite webSite = WebSiteFactory.createWebSiteStandard("Mario", "Rossi", "50");

        webSite.showPage();
        webSite.operation(22);
        webSite.operation(42);
    }
}
