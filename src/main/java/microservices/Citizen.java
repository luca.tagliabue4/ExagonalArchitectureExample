package microservices;

public class Citizen implements IPerson {

    private final String firstName;
    private final String lastName;
    private final String age;

    public Citizen(String f, String l, String a){
        firstName = f;
        lastName = l;
        age = a;
    }
    @Override
    public String firstName() {
        return firstName;
    }

    @Override
    public String lastName() {
        return lastName;
    }

    @Override
    public String age() {
        return age;
    }
}
