package microservices;

public class DashGUI implements IGui {
    @Override
    public void print(String content, int numDashes) {

        for(int i = 0; i < numDashes; i++){
            System.out.print("-");
        }
        System.out.println("");

        System.out.println(content);

        for(int i = 0; i < numDashes; i++){
            System.out.print("-");
        }
        System.out.println("");
    }
}
