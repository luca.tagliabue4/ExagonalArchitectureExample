package microservices;

public class DummyTrigger implements ITrigger{
    @Override
    public int getId() {
        return 22;
    }

    @Override
    public void execute() {
        System.out.println("TRIGGER EXECUTED !!!!!");
    }
}
