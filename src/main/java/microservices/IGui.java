package microservices;

public interface IGui {
    void print(String content, int numDashes);
}
