package microservices;

public interface IPerson {
    String firstName();
    String lastName();
    String age();
}
