package microservices;

public interface ITrigger {
    int getId();
    void execute();
}
